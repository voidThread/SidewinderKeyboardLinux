//
// Created by dawid on 02.03.17.
//
#pragma once

#include <cstdint>
struct {
  const uint16_t IdVendor = 0x045e; //Microsoft Corp.
  const uint16_t IdProduct = 0x0768; //SidewinderX4

  const unsigned char InterfaceNumbMainKeyboard = 0;
  const unsigned char InterfaceNumbMacroKeys = 1;
} SidewinderX4Const;