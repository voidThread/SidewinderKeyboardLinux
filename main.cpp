#include <stdio.h>
#include <libusb-1.0/libusb.h>
#include <iostream>
#include <array>
#include "deviceConsts.h"

int main() {
  const auto endpointIn = 0x81; //main keyboard keys
  const auto endpointInMacros = 0x83; //macro keys
  const auto endpointOut = 0x01;  //for output

  const auto MAX_SIZE = 64; //max array size
  auto current_size = 0;  //current data size
  std::array<unsigned char, MAX_SIZE> data {0}; //buffer array
  auto result = -1; //result from interrupt transfers

  libusb_device** devicesList;  //device list
  libusb_context* ctx;  //context

  /*
   * If context was successful initialize, then start all procedure to get communication with macro keys.
   */
  if (libusb_init(&ctx) == LIBUSB_SUCCESS) {                                                                            //init libusb on specific context
    libusb_device_handle *keyboardHandle{nullptr};  //inital keyboard handle

    int returnValue = libusb_get_device_list(ctx, &devicesList);

    /*
     * Start search specific keyboard
     */
    if (returnValue > 0) {                                                                                              //if get device list success
      for (int i = 0; i < returnValue; ++i) { //search device loop
        libusb_device *device = devicesList[i]; //get specific device
        libusb_device_descriptor descriptor;  //device descriptor

        int ret = libusb_get_device_descriptor(device, &descriptor);

        if (ret == LIBUSB_SUCCESS) {                                                                                    //if success get descriptor

          /*
           * Device is Microsoft Sidewinder X4
           */
          if (descriptor.idVendor == SidewinderX4Const.IdVendor && descriptor.idProduct == SidewinderX4Const.IdProduct) {
            int returnOpenDeviceCode = libusb_open(device, &keyboardHandle);

            if (returnOpenDeviceCode != LIBUSB_SUCCESS) {                                                               //problem with opening device
              std::cerr << "Problem with open device! Code: " << returnOpenDeviceCode << "\n"
                        << libusb_error_name(returnOpenDeviceCode) << std::endl;

              return 1;
            }
          }
        } else {                                                                                                        //problem to get device descriptor
          std::cerr << "Cannot get device descriptor! Code: " << ret << "\n"
                    << libusb_error_name(ret) << std::endl;
          return 1;
        }
      }
    }

    libusb_free_device_list(devicesList, 0);                                                                            //this last line with search procedure

    /*
     * Next step is try claim interfaces
     * For us interesting is interface from macro keys
     * If you want check this numbs try
     * lsusb -v -d 045e:0768
     * bInterfaceNumber & bEndpointAddress
     */
    if (keyboardHandle != nullptr) {
      /*
       * Enable auto detach kernel driver, without it driver will be dangling
       * and keyboard will not respond
       */
      if (libusb_set_auto_detach_kernel_driver(keyboardHandle, 1) == LIBUSB_SUCCESS) {
        if (libusb_claim_interface(keyboardHandle, SidewinderX4Const.InterfaceNumbMacroKeys) == LIBUSB_SUCCESS) {       //claim and select right interface

          /*
           * ===============Main loop===============
           * For now communication is synchronous, wait 2 sec and interrupt
           * If current size of sent package is zero, then break loop.
           */
          do {
            result = libusb_interrupt_transfer(keyboardHandle, endpointInMacros, &data[0], MAX_SIZE, &current_size, 2000);

            printf("Transferred: %i \n", current_size);

            for (auto i = 0; i < current_size; i++) {
              printf("%i", data.at(i));
            }
          }while (current_size != 0);

          libusb_release_interface(keyboardHandle, 1);
        }
      }
      libusb_close(keyboardHandle);
    }

    libusb_exit(ctx);
  }
  return 0;
}